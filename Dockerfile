FROM debian:buster
MAINTAINER Jiří Fejfar <jurafejfar@gmail.com> 

RUN apt update && apt dist-upgrade -y
RUN useradd --create-home --shell /bin/bash myuser
RUN echo myuser:mypwd | chpasswd

RUN apt install sudo -y
RUN usermod -a -G sudo myuser

RUN apt install -y git clang libreadline-dev libz-dev make bison flex

# Set the working directory to app home directory
WORKDIR /home/myuser

# Specify the user to execute all commands below
USER myuser

RUN git clone https://git.postgresql.org/git/postgresql.git

#RUN cd postgresql
#RUN git pull
#RUN CC=clang-7 ./configure --prefix=/home/myuser/pg-master --enable-debug CFLAGS="-O0 -ggdb3" --enable-cassert
#RUN make -s -j4
#RUN make -s install
#RUN export PATH=/home/myuser/pg-master/bin:$PATH

