reproducer extension
====================

https://www.postgresql.org/message-id/15616-260dc9cb3bec7e7e@postgresql.org

2) Hanging OID in extconfig
Related issue. If the table (or sequence) was assigned to dump by
pg_extension_config_dump, its OID is still present in the field "extconfig"
of pg_extension after it was removed by DROP TABLE.
