select count(*) as number_of_objects_in_pg_extension from 
	(select unnest(extconfig) from pg_extension where extname = 'reproducer') as foo;

select count(*) as number_of_objects_in_pg_class from pg_class
	where oid IN  
	(select unnest(extconfig) as extconfig_oid from pg_extension where extname = 'reproducer')
;
