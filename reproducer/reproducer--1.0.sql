/* reproducer--1.0.sql */

-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION reproducer" to load this file. \quit

create table sometable (a int, b int);
select pg_catalog.pg_extension_config_dump('sometable', '');
