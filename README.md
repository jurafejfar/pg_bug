# README

create docker image
```bash
docker build -t registry.gitlab.com/jurafejfar/pg_bug .
docker login registry.gitlab.com/jurafejfar/pg_bug
docker push registry.gitlab.com/jurafejfar/pg_bug
```

run, work with & destroy image
```bash
docker images
docker run -i -t -v `pwd`:/home/myuser/host_work_dir --name pgcontainer registry.gitlab.com/jurafejfar/pg_bug /bin/bash
docker container ls -a
docker start pgcontainer
docker attach pgcontainer
docker container rm -v pgcontainer
```

run CI/CD jobs locally
```bash
gitlab-runner exec docker build1
gitlab-runner exec docker --docker-volumes `pwd`/build-output:/builds test1
```

clean-up
```bash
docker rm `docker ps -aq`
docker system prune -a
```
